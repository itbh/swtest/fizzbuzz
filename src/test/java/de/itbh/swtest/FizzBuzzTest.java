package de.itbh.swtest;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;



class FizzBuzzTest {

    @Test
    void testOneReturns1() {
        String actual = FizzBuzz.calc(1);

        assertEquals("1",actual);
    }

    @Test
    void testTwoReturns2() {
        String actual = FizzBuzz.calc(2);
        assertEquals("2", actual);
    }

    @ParameterizedTest
    @CsvSource({"1,1", "2,2","3,FIZZ"})
    void parameterizedTest(int number, String expected) {
        String actual = FizzBuzz.calc(number);

        assertEquals(expected, actual);
    }
}