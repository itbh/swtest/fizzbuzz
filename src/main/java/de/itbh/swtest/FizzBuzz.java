package de.itbh.swtest;

/**
 * FIZZBUZZ
 *
 * Zahl durch 3 teilbar: Fizz
 * Zahl durch 5 teilbar: Buzz
 * Zahl durch 3 und 5 teilbar: FizzBuzz
 */

public class FizzBuzz {

    public static void main(String[] args) {
        System.out.println(FizzBuzz.calc(1));
    }

    /**
     * Converts a given number into a String representation
     * @param number given Number
     * @return Number as String
     */
    public static String calc(int number) {
        return String.valueOf(number);
    }
}
